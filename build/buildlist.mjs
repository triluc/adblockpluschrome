/*
 * This file is part of Adblock Plus <https://adblockplus.org/>,
 * Copyright (C) 2006-present eyeo GmbH
 *
 * Adblock Plus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Adblock Plus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.
 */

import argparse from "argparse";
import fs from "fs";
import handlebars from "handlebars";
import path from "path";

const FILENAME = "build_list.json";
const MAXKEPTBUILDS = 150;
const TEMPLATE_FILENAME = "./build/templates/distributions.html.tmpl";
const TARGET_LIST_FILENAME = "index.html";
const BUILD_LABELS = {
  chrome: "Chromium",
  gecko: "Firefox",
  source: "Source archive"
};

handlebars.registerHelper("dictsize", (dict, key, opts) =>
{
  return Object.keys(dict).length;
});

handlebars.registerHelper("buildlabel", (type, key, opts) =>
{
  return BUILD_LABELS[type];
});

handlebars.registerHelper("cellTag", (data, key, opts) =>
{
  let classes = [];
  if (key.data.first)
    classes.push("t_border_top");
  if (key.data.last)
    classes.push("t_border_bot");
  if (data.channel == "release")
    classes.push("extra_bold");
  if (data.channel == "unsigned")
    classes.push("extra_light");

  return new handlebars.SafeString(`<td class="${classes.join(" ")}">`);
});

function currentTime()
{
  return new Date().toISOString().replace("T", " ").replace(/:[^:]+$/, "");
}

async function sizeOf(filename)
{
  return ((await fs.promises.stat(filename)).size / 1000000.0).toFixed(2);
}

export class BuildList
{
  /**
   * Create a new BuildList object.
   * @param {Object} args - the parsed command line arguments.
   * @return {BuildList} an initialized BuildList object
   */
  static async getInstance(args)
  {
    let content;
    let targetFolder = args.working_directory;
    let filename = path.join(targetFolder, FILENAME);
    try
    {
      content = JSON.parse(await fs.promises.readFile(filename));
    }
    catch (err)
    {
      console.warn(`WARNING: Could not read ${filename}`);
      content = {commits: [], builds: {}};
    }

    return new BuildList(args, content, targetFolder, filename);
  }

  constructor(args, content, targetFolder, filename)
  {
    this.args = args;
    this.content = content;
    this.targetFolder = targetFolder;
    this.filename = filename;
  }

  write()
  {
    return fs.promises.writeFile(
      this.filename,
      JSON.stringify(this.content)
    );
  }

  /**
   * Check if a version is already listed in the cached build_list.json.
   * @param {string} version - the version to search for.
   * @param {string} type - the platform we are currently building for.
   * @param {string} channel - the release level / channel of the build.
   * @return {boolean} whether the version exists or not.
   */
  versionExists(version, type, channel)
  {
    for (let commit of Object.keys(this.content.builds))
    {
      for (let build of this.content.builds[commit])
      {
        if (build.type == type &&
            build.version == version &&
            build.channel == channel)
          return true;
      }
    }
    return false;
  }

  /**
   * Sort a new build into the cached list of builds, grouped by commit and
   * drop the `lowest` one.
   * @param {Object} build - the new build to add
   * @param {string} build.commit - the build's commit that should be cycled
   * into the list
   * @param {string} build.channel - the build's release channel.
   * @param {string} build.type - the new build's platform.
   * @param {string} build.version - the new file's version.
   * @param {string} build.filename - the new file's base name.
   * @param {string} build.downloadUrl - the new file's absolute URL.
   * @param {string} build.timeCreated - the creation time of the new file.
   * @param {string} build.fileSize - the size of the file in MB.
   */
  cycleBuilds({commit, channel, type, version, filename, downloadUrl,
               timeCreated, fileSize})
  {
    if (commit == "" || commit == "undefined")
    {
      console.warn(`Ignoring empty commit for ${filename} from ` +
                   `${timeCreated}`);
      return;
    }

    function cmp(left, right)
    {
      for (let accessor of ["version", "timeCreated", "type", "fileSize"])
      {
        let comparal = left[accessor].localeCompare(right[accessor]);
        if (comparal != 0)
          return comparal;
      }
      return 0;
    }

    if (!this.content.commits.includes(commit))
    {
      this.content.commits.unshift(commit);
      this.content.builds[commit] = [];
    }

    this.content.builds[commit].push({
      filename,
      channel,
      version,
      downloadUrl,
      timeCreated,
      fileSize,
      type
    });

    this.content.builds[commit].sort(cmp).reverse();

    this.content.commits.sort((left, right) => cmp(
      this.content.builds[left][0],
      this.content.builds[right][0]
    )).reverse();

    for (let oldCommit of this.content.commits.splice(MAXKEPTBUILDS))
      delete this.content.builds[oldCommit];
  }

  /**
   * Merge a single build's metadata from a .json file (as generated by
   * writeBuildMetadata()) into this object.
   * @param {string} filename - The source file.
   */
  async cycleBuildFromFile(filename)
  {
    let data = JSON.parse(await fs.promises.readFile(filename));
    if (!this.versionExists(data.version, data.type, data.channel))
      this.cycleBuilds(data);
  }

  /**
   * Write a single build's metadata to a .json file.
   * @param {string} data - the builds metadata (@see {@link cycleBuilds}).
   * @return {Promise} rejectable / resolvable promise about the write
   * operation.
   */
  async writeBuildMetadata(data)
  {
    let targetFilename = `new-${this.args.channel}-${this.args.platform}-` +
                         `${this.args.revision}.json`;
    let metadata = {
      commit: this.args.revision,
      channel: this.args.channel,
      platform: this.args.platform,
      ...data,
      fileSize: await sizeOf(data.filename),
      timeCreated: currentTime(),
      downloadUrl: this.getArtifactUrl(data.filename)
    };
    return fs.promises.writeFile(targetFilename, JSON.stringify(metadata));
  }

  /**
   * Render this objects data into a representative HTML page `index.html`.
   * @param {string} projectUrl - The project's base url for for following a
   * commit's history.
   * @return {Promise} rejectable / resolvable promise about the rendering
   * operation.
   */
  async render(projectUrl)
  {
    let templateData = await fs.promises.readFile(TEMPLATE_FILENAME, "utf-8");
    let template = handlebars.compile(templateData, {strict: true});
    let context = this.content;
    context["projectUrl"] = projectUrl;

    return fs.promises.writeFile(
      path.join(this.targetFolder, TARGET_LIST_FILENAME),
      template(context)
    );
  }

  /**
   * Parse a version number from a filename.
   * @param {string} filename - the filename to parse.
   * @return {string} the parsed version or null.
   */
  static versionFromFilename(filename)
  {
    let reg = new RegExp(/(\d+(\.\d+){0,3}(\.\*)?)/);
    let match = filename.match(reg);
    if (match)
      return match[0];
    return null;
  }

  /**
   * Add shared standard arguments to an argparse.ArgumentParser instance,
   * providing default values available from the GitLab CI environment.
   * @param {argparse.ArgumentParser} parser - the ArgumentParser instance.
   */
  static addStandardArguments(parser)
  {
    parser.addArgument(
      "--revision",
      {
        help: "Current revision SHA. Defaults to $CI_COMMIT_SHORT_SHA @ CI.",
        defaultValue: process.env.CI_COMMIT_SHORT_SHA
      }
    );
    parser.addArgument(
      "--root-url",
      {
        help: "GitLab API V4 root URL. Defaults to $CI_API_V4_URL @ CI.",
        defaultValue: process.env.CI_API_V4_URL
      }
    );
    parser.addArgument(
      "--project-id",
      {
        help: "This project's GitLab ID. Defaults to $CI_PROJECT_ID @ CI.",
        defaultValue: process.env.CI_PROJECT_ID
      }
    );
    parser.addArgument(
      "--job-id",
      {
        help: "The current CI job's GitLab ID. Defaults to $CI_JOB_ID @ CI.",
        defaultValue: process.env.CI_JOB_ID
      }
    );
    parser.addArgument(
      "--working-directory",
      {
        help: "Directory to read/write the build_list.json from/to. Defaults" +
              " to $DISTRIBUTIONS_DIRECTORY @ CI.",
        defaultValue: process.env.DISTRIBUTIONS_DIRECTORY
      }
    );
  }

  /**
   * Construct a URL to download a package from GitLab artifacts, according to
   * the provided parameters.
   * @param {string} filename - the resulting file's name.
   * @return {string} URL to download the file from.
   */
  getArtifactUrl(filename)
  {
    return `${this.args.root_url}/projects/${this.args.project_id}/jobs/` +
           `${this.args.job_id}/artifacts/${filename}`;
  }

  /**
   * Add additional args required when creating new metadata.
   * @param {argparse.ArgumentParser} parser - the ArgumentParser instance.
   */
  static addNewMetaMandatoryArgs(parser)
  {
    parser.addArgument(
      ["-p", "--platform"],
      {
        help: "Target platform",
        choices: ["gecko", "chrome"],
        defaultValue: "chrome"
      }
    );
    parser.addArgument(
      ["-c", "--channel"],
      {
        help: "Handled channel.",
        choices: ["release", "usigned", "source"],
        defaultValue: "unsigned"
      }
    );
  }

  /**
   * Parse command line arguments and construct a BuildList instance with an
   * according state.
   * @param {string} description - The verbose description the ArgumentParser
   *   should print to STDOUT when required.
   * @param {boolean} newMetaArgs - whether or not the instanciated
   *   ArgumentParser handles additional parameters for new metadata files.
   * @param {...Object} additionalArgs - Arbitrary additional parameters the
   *   ArgumentParser should parse.
   * @return {BuildList} the instantiated BuildList object.
   */
  static async parseArgs(description, newMetaArgs, ...additionalArgs)
  {
    let parser = new argparse.ArgumentParser({description});

    this.addStandardArguments(parser);
    if (newMetaArgs)
      this.addNewMetaMandatoryArgs(parser);

    for (let addArg of additionalArgs)
      parser.addArgument(...addArg);

    return await this.getInstance(parser.parseArgs());
  }
}
