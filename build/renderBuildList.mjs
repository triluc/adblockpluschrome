/*
 * This file is part of Adblock Plus <https://adblockplus.org/>,
 * Copyright (C) 2006-present eyeo GmbH
 *
 * Adblock Plus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Adblock Plus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.
 */

import {BuildList} from "./buildlist.mjs";

(async() =>
{
  let builds = await BuildList.parseArgs(
    "Render a HTML page with a list of available distributions.",
    false,
    [
      "--project-url",
      {
        help: "URL of the project, which concatenated with a commit links to" +
              " that commits history.",
        defaultValue: `${process.env.CI_PROJECT_URL}/-/commits/`
      }
    ],
    [
      "mergeFiles",
      {
        nargs: "*",
        help: "Additional .json files, listing a single build each, that " +
              "should be merged into the build_list.json"
      }
    ]
  );

  try
  {
    for (let file of builds.args.mergeFiles)
      await builds.cycleBuildFromFile(file);

    builds.write();
    builds.render(builds.args.project_url);
  }
  catch (err)
  {
    console.error(err);
    process.exit(1);
  }
})();
