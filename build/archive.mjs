/*
 * This file is part of Adblock Plus <https://adblockplus.org/>,
 * Copyright (C) 2006-present eyeo GmbH
 *
 * Adblock Plus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Adblock Plus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.
 */

import fs from "fs";
import path from "path";
import got from "got";
import {BuildList} from "./buildlist.mjs";

(async() =>
{
  let builds = await BuildList.parseArgs(
    "Download an Adblock Plus release build.",
    true,
    ["extension-id", {help: "The id of the extension."}]
  );

  let dirname = path.join("build", "downloadInfo");

  let module = await import(path.resolve(dirname,
                                         `${builds.args["platform"]}.mjs`));

  let version = null;
  let filename = null;

  let downloadUrl = await module.getLatestFileUrl(builds.args["extension-id"]);
  let stream = got.stream(downloadUrl);

  stream.on("response", response =>
  {
    let remoteFilename = path.basename(response.req.path);
    filename = module.filenameFormat(remoteFilename);
    version = BuildList.versionFromFilename(filename);

    if (builds.versionExists(version, builds.args.platform,
                             builds.args.channel))
    {
      console.warn(`Version ${version} already exists. Aborting.`);
      process.exit();
    }

    stream.pipe(fs.createWriteStream(filename));
  });

  stream.on("end", response =>
  {
    return builds.writeBuildMetadata({version, filename});
  });

  stream.on("error", error =>
  {
    console.error(error);
    process.exit(1);
  });
})();
