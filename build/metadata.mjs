/*
 * This file is part of Adblock Plus <https://adblockplus.org/>,
 * Copyright (C) 2006-present eyeo GmbH
 *
 * Adblock Plus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Adblock Plus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.
 */

import path from "path";
import {BuildList} from "./buildlist.mjs";

(async() =>
{
  let builds = await BuildList.parseArgs(
    "Download an Adblock Plus build from the Chrome Web Store.",
    true,
    ["package", {help: "The unsigned build package."}]
  );

  let filename = path.basename(builds.args.package);

  let version = BuildList.versionFromFilename(filename);
  if (version == null)
  {
    console.error(`ERROR: The filename ${filename} could not be parsed.`);
    process.exit(1);
  }

  if (builds.versionExists(version, builds.args.platform, builds.args.channel))
  {
    console.warn(`Version ${version} already exists. Aborting.`);
    process.exit();
  }

  return builds.writeBuildMetadata({version, filename});
})();
